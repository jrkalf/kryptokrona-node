# // Copyright (c) 2012-2017, The CryptoNote developers, The Bytecoin developers
# // Copyright (c) 2014-2018, The Monero Project
# // Copyright (c) 2018-2019, The TurtleCoin Developers
# // Copyright (c) 2019, The Kryptokrona Developers
# //
# // Please see the included LICENSE file for more information.

# this docker file can be used for both a daemon, miner and a wallet if needed
# we only have to set specific command or entrypoint inside the docker compose
# file to customize the behavior of the container when started.

ARG ARCH=
FROM ${ARCH}ubuntu:22.04 as build

# install build dependencies
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y \
      build-essential \
      libssl-dev \
      libffi-dev \
      python3-dev \
      gcc-11 \
      g++-11 \
      git \
      cmake \
      librocksdb-dev \
      libboost-all-dev \
      libboost-system1.74.0 \
      libboost-filesystem1.74.0 \
      libboost-thread1.74.0 \
      libboost-date-time1.74.0 \
      libboost-chrono1.74.0 \
      libboost-regex1.74.0 \
      libboost-serialization1.74.0 \
      libboost-program-options1.74.0 \
      libicu70

WORKDIR /usr/src
RUN git clone https://github.com/kryptokrona/kryptokrona.git
WORKDIR /usr/src/kryptokrona

# create the build directory
RUN mkdir build
WORKDIR /usr/src/kryptokrona/build

# build and install
RUN cmake -DCMAKE_CXX_FLAGS="-g0 -O3 -fPIC -std=gnu++17" .. \
    && make -j$(nproc) --ignore-errors kryptokronad

###
FROM ${ARCH}ubuntu:22.04

ENV PATH "$PATH:/kryptokrona"
ENV NODE_ARGS ""

# Exposing miner-to-node port
EXPOSE 11898

WORKDIR /kryptokrona

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get -y install \
        libffi8 \
        libssl3 \
        librocksdb6.11 \
        libboost-system1.74.0 \
        libboost-filesystem1.74.0 \
        libboost-thread1.74.0 \
        libboost-date-time1.74.0 \
        libboost-chrono1.74.0 \
        libboost-regex1.74.0 \
        libboost-serialization1.74.0 \
        libboost-program-options1.74.0 \
        libicu70 \
    && rm -rf /var/lib/apt/lists/*

# create the directory for the daemon files
RUN mkdir -p /kryptokrona/blockloc

COPY --from=build /usr/src/kryptokrona/start.sh .
COPY --from=build /usr/src/kryptokrona/build/src/kryptokronad .
RUN chmod +x start.sh kryptokronad

# --data-dir is binded to a volume - this volume is binded when starting the container
# to start the container follow instructions on readme or in article published by marcus cvjeticanin on https://mjovanc.com
CMD [ "/bin/sh", "-c", "./kryptokronad --enable-cors=* --rpc-bind-ip=127.0.0.1 --rpc-bind-port=11898 ${NODE_ARGS}" ]
